//
//  TechNewsProTests.swift
//  TechNewsProTests
//
//  Created by Subash Parajuli on 11/22/20.
//

import XCTest
@testable import TechNewsPro

class TechNewsProTests: XCTestCase {
    
    let networkAdapter = NetworkAdapter.shared
    
    func testForNewsList() {
        let newsId = 1
        var newsResponse: [News]? = nil
        let newsExpectation = expectation(description: "news list")
        networkAdapter.getNewsListInCategory(id: newsId, success: {(successInfo) in
            newsResponse = successInfo.news
            newsExpectation.fulfill()
            
        }, failure: {(error) in})
        
        self.waitForExpectations(timeout: 5.0) { _ in
            print(newsResponse as Any)
            XCTAssertNotNil(newsResponse)
        }
        
    }

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
