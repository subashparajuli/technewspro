//
//  CellsExtensions.swift
//  TechNewsPro
//
//  Created by Subash Parajuli on 11/22/20.
//

import UIKit

extension UICollectionViewCell {
    func asNib() -> UINib {
        return UINib(nibName: Self.description(), bundle: nil)
    }
}
