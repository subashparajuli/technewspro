//
//  NewsModel.swift
//  TechNewsPro
//
//  Created by Subash Parajuli on 11/22/20.
//

//Models generated from http://www.json4swift.com

import Foundation
struct NewsCategory : Codable {
    let id : Int?
    let name : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }

}


struct NewsForCategory : Codable {
    let news : [News]?

    enum CodingKeys: String, CodingKey {

        case news = "news"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        news = try values.decodeIfPresent([News].self, forKey: .news)
    }

}

struct News : Codable {
    let title : String?
    let description : String?
    let imgUrl : String?
    let link : String?

    enum CodingKeys: String, CodingKey {

        case title = "title"
        case description = "description"
        case imgUrl = "imgUrl"
        case link = "link"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        imgUrl = try values.decodeIfPresent(String.self, forKey: .imgUrl)
        link = try values.decodeIfPresent(String.self, forKey: .link)
    }

}
