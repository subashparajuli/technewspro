//
//  NetworkAdapter.swift
//  TechNewsPro
//
//  Created by Subash Parajuli on 11/22/20.
//

import Foundation
import Moya


protocol Network {
    associatedtype T: TargetType
    var provider: MoyaProvider<T> { get }
}

class NetworkAdapter {
    
    static let shared = NetworkAdapter()
  
    var requests: [Cancellable] = []
    
    let provider = MoyaProvider<TechNews>(plugins: [NetworkLoggerPlugin()])
    
    func getNewsCategory(success: @escaping( _ newsCat: [NewsCategory]) -> (), failure: @escaping(_ error: Error) -> ()) {
        
        provider.request(.newsCategoty) { result in
            switch result {
            case let .success(successResponse):
//                let statusCode = successResponse.statusCode
                let data = successResponse.data
                
                do {
                    
                    let catInfo = try JSONDecoder().decode([NewsCategory].self, from: data)
                    
                    success(catInfo)
                    
                } catch {
                    print("decode error",error)
                }
                
                
            case let .failure(error):
                debugPrint(error.localizedDescription)
                failure(error)
            }
        }
        
    }
    
    func getNewsListInCategory(id: Int, success: @escaping(_ newsList: NewsForCategory) ->(), failure: @escaping(_ error: Error ) ->()) {
        
        provider.request(.newsForCategory(id: id)) { result in
            switch result {
            case let .success(successResponse):
//                let statusCode = successResponse.statusCode
                let data = successResponse.data
                
                do {
                    
                    let newsListInfo = try JSONDecoder().decode(NewsForCategory.self, from: data)
                    
                    success(newsListInfo)
                    
                } catch {
                    print("decode error",error)
                }
                
                
            case let .failure(error):
                debugPrint(error.localizedDescription)
                failure(error)
            }
        }

        
        
        
    }
    
    
}
