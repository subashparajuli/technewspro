//
//  TechNewsEndPoints.swift
//  TechNewsPro
//
//  Created by Subash Parajuli on 11/22/20.
//

import Foundation
import Moya

enum TechNews {
    case newsCategoty, newsForCategory(id: Int)
}

extension TechNews: TargetType {
    
    var baseURL: URL {
        guard let url = URL(string: "https://life-hacks-pro.herokuapp.com/") else { fatalError("Base url could not be configured") }
        return url
    }
    
    var path: String {
        switch self {
        case .newsCategoty:
            return "news/sources"
        case .newsForCategory(let id):
            return "news/\(id)"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .newsCategoty, .newsForCategory:
            return .get
        
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .newsCategoty, .newsForCategory:
            return .requestPlain
//        case .newsForCategory:
//            return .requestParameters(parameters: ["id": 1], encoding:    URLEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        return .none
    }
    
   
    
    
}
