//
//  NewsCollectionView.swift
//  TechNewsPro
//
//  Created by Subash Parajuli on 11/22/20.
//

import UIKit

class NewsCollectionViewCell: UICollectionViewCell {
    
    //MARK:- IBOutlets for cell
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var labelNewsTitle: UILabel!
    @IBOutlet weak var labelNewsDescription: UILabel!
    
    //MARK:- Variable for cells
    override class func description() -> String {
        return "NewsCollectionViewCell"
    }
    
    //MARK:- Lifecycle for cell
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //view setups
        self.containerView.setCornerRadius(radius: 8)
        self.newsImage.setCornerRadius(radius: 8)
        self.newsImage.setShadow(shadowColor: UIColor.darkGray, shadowOpacity: 1, shadowRadius: 5, offset: CGSize(width: 0, height: 2))
        self.containerView.setShadow(shadowColor: UIColor.darkGray, shadowOpacity: 2, shadowRadius: 8, offset: CGSize(width: 0, height: 5))
        
        //MARK:- TODO
        //Add shimmer effects
        
    }



}
