//
//  NewsHomeVC.swift
//  TechNewsPro
//
//  Created by Subash Parajuli on 11/22/20.
//

import UIKit
import Nuke
import SafariServices


class NewsHomeVC: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var cat1View: UIView!
    @IBOutlet weak var cat1Title: UILabel!
    @IBOutlet weak var cat1ButtonShowMore: UIButton!
    @IBOutlet weak var collectionView1: UICollectionView!
    
    
    @IBOutlet weak var cat2View: UIView!
    @IBOutlet weak var cat2Title: UILabel!
    @IBOutlet weak var cat2ButtonShowMore: UIButton!
    @IBOutlet weak var collectionView2: UICollectionView!
    
    
    @IBOutlet weak var cat3View: UIView!
    @IBOutlet weak var cat3Title: UILabel!
    @IBOutlet weak var cat3ButtonShowMore: UIButton!
    @IBOutlet weak var collectionView3: UICollectionView!
    
    
    //MARK:- Variable Declarations
    private let newsCategoryViewModel = NewsCategoryViewModel()
    private let newsListViewModel = NewsListViewModel()
    private let networkAdapter = NetworkAdapter.shared
    
    private var newsList1: [News]?
    
    
    
    
    //MARK:- View Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //Register nib to collectionview
        self.setCollectionView(cv: collectionView1)
        self.setCollectionView(cv: collectionView2)
        self.setCollectionView(cv: collectionView3)
        
        
        //feth data from api
        self.getNewsCategory()
        self.getNewsListInCategory(id: 1)
        
        
        
    }
    
    //MARK:- Setups
    fileprivate func setCollectionView(cv: UICollectionView) {
        
        cv.delegate = self
        cv.dataSource = self
        cv.register(NewsCollectionViewCell().asNib(), forCellWithReuseIdentifier: "NewsCollectionViewCell")
        cv.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        cv.contentInsetAdjustmentBehavior = .never
        
        
        
    }
    
    
    
    
    //MARK:- API Calls
    
    ///fetching news category for collectionview header
    private func getNewsCategory() {
        newsCategoryViewModel.getNewsCategory(networkAdapter: networkAdapter)
        
        newsCategoryViewModel.updateLoadingStatus = {
            //show loading indicator or any message
            
        }
        
        newsCategoryViewModel.didFinishFetch = {
            //currently only the first three titles are fetched
            self.cat1Title.text = self.newsCategoryViewModel.newsCategory[0].name
            self.cat2Title.text = self.newsCategoryViewModel.newsCategory[1].name
            self.cat3Title.text = self.newsCategoryViewModel.newsCategory[2].name
            
            
        }
        
        newsCategoryViewModel.showAlertClosure = {
            //if any error show them here
        }
        
    }
    
    
    ///fetching newslist in each category
    private func getNewsListInCategory(id: Int) {
        
        newsListViewModel.getNewsListInCategory(id: id, networkAdapter: networkAdapter)
        
        newsListViewModel.updateLoadingStatus = {
            //show loading indicator or any message
            
        }
        
        newsListViewModel.didFinishFetch = { [self] in
            //update views here
            self.newsList1 = self.newsListViewModel.newsList
            
            
            #warning("Fetch different datasource for different collectionview")
            self.collectionView1.reloadData()
            self.collectionView2.reloadData()
            self.collectionView3.reloadData()
            
            
        }
        
        newsListViewModel.showAlertClosure = {
            //if any error show them here
        }
        
    }
    
    
    //MARK:- Safari View Controller
    func openUrlWithLink(urlString: String) {
        
        guard let url = URL(string: urlString) else { return }
        print("open url")
        
        if !["http", "https"].contains(url.scheme?.lowercased() ?? "") {
            // Can open with SFSafariViewController
            let appendedLink = "http://".appendingFormat(urlString)  //.stringByAppendingString(url)
            guard let appendedUrl = URL(string: appendedLink) else { return }
            let safariViewController = SFSafariViewController(url: appendedUrl)
            safariViewController.delegate = self
            self.present(safariViewController, animated: true, completion: nil)
            
        } else {
            let safariViewController = SFSafariViewController(url: url)
            safariViewController.modalPresentationStyle = .overCurrentContext
            self.present(safariViewController, animated: true, completion: nil)
        }
        
        
    }
    
}


//MARK:- Collection view datasource and delegates
extension NewsHomeVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView {
        case collectionView1, collectionView2, collectionView3:
            return self.newsListViewModel.isLoading ? 1 : self.newsListViewModel.newsList.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        #warning("Fetch different datasource for different collectionview")
        
        switch collectionView {
        case collectionView1, collectionView2, collectionView3:
            let cell: NewsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsCollectionViewCell", for: indexPath) as! NewsCollectionViewCell
            cell.labelNewsTitle.text = newsList1?[indexPath.item].title
            cell.labelNewsDescription.text = newsList1?[indexPath.item].description
            let imageUrlString = newsList1?[indexPath.item].imgUrl ?? ""
            
            if let imageUrl = URL(string: imageUrlString) {
                Nuke.loadImage(with: imageUrl,
                               options: ImageLoadingOptions(placeholder: UIImage(named: "noimage.png"), transition: .fadeIn(duration: 0.33)),
                               into: cell.newsImage)
            }
            
            return cell
            
            
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch collectionView {
        case collectionView1, collectionView2, collectionView3:
            return CGSize(width: self.collectionView1.frame.width, height: self.collectionView1.frame.height)
        default:
            return .zero
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let newsLink = newsList1?[indexPath.item].link else {
            //show error here
            return
            
        }
        openUrlWithLink(urlString: newsLink)
        
    }
    
    
}


//MARK:- Safari View Controller Delegates
extension NewsHomeVC: SFSafariViewControllerDelegate {
    
}
