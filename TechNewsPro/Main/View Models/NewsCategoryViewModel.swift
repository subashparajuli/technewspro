//
//  NewsCategoryViewModel.swift
//  TechNewsPro
//
//  Created by Subash Parajuli on 11/22/20.
//

import Foundation

class NewsCategoryViewModel {
    var newsCategory: [NewsCategory]! {
        didSet {
            self.didFinishFetch?()
        }
    }
    
    var error: Error? {
        didSet { self.showAlertClosure?() }
    }
    var isLoading: Bool = true {
        didSet { self.updateLoadingStatus?() }
    }
    
    var showAlertClosure: (() -> ())?
    var updateLoadingStatus: (() -> ())?
    var didFinishFetch: (() -> ())?
    
    func getNewsCategory(networkAdapter: NetworkAdapter) {
        networkAdapter.getNewsCategory( success: {(successInfo) in
            self.isLoading = false
            self.error = nil
            self.newsCategory = successInfo
            
        }, failure: {(error) in
            self.isLoading = false
            self.error = error
            
        })
    }
    
    
    
}
