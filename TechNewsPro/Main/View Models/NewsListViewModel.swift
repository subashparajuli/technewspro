//
//  NewsViewModel.swift
//  TechNewsPro
//
//  Created by Subash Parajuli on 11/22/20.
//

import Foundation


class NewsListViewModel {
    var newsList: [News]! {
        didSet {
            self.didFinishFetch?()
        }
    }
    
    var error: Error? {
        didSet { self.showAlertClosure?() }
    }
    var isLoading: Bool = true {
        didSet { self.updateLoadingStatus?() }
    }
    
    var showAlertClosure: (() -> ())?
    var updateLoadingStatus: (() -> ())?
    var didFinishFetch: (() -> ())?
    
    func getNewsListInCategory(id: Int, networkAdapter: NetworkAdapter) {
        networkAdapter.getNewsListInCategory(id: id, success: {(successInfo) in
            self.isLoading = false
            self.error = nil
            self.newsList = successInfo.news
            
        }, failure: {(error) in
            self.isLoading = false
            self.error = error
            
        })
    }
    
    
    
}
